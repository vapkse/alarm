/*
Alarm
 Version 2.0.1
 Date: 05.05.2015
 */
#include <avr/sleep.h>
#include <avr/power.h> //Needed for powering down perihperals such as the ADC/TWI and Timers
#include <avr/wdt.h>
#include <Wire.h>
#include <math.h>
#include <Blink.h>

// User values
#define debug false
#define ledEnabled true
#define hasAlarm1 true          // Inboard motion detector 
#define hasAlarm2 false
#define hasAlarm3 false
#define hasAlarm4 false         // Button and alarm 4 are on the same pin and can't be used together
#define hasButton false         // Button and alarm 4 are on the same pin and can't be used together
#define hasDoorContact false
#define hasLightSensor false
#define enableBatteryTest false
#define dailyCallDelay 15000    // Seconds after day light
#define smsDelayTime 15         // Seconds before sending another sms
#define buttonPressMinTime 80   // Debounce push button time in milliseconds
#define ledOnAlarmMinTime 20    // Time in seconds during the LED stay on if the alarm is disabled and a captor raise an alarm
#define enablingMinTime 300     // Enabling time in seconds after the push button is pressed
#define detectedMinTime 20      // Waiting time in seconds after a detection before calling
#define calledMinTime 3600      // Time in seconds before the alarm start again after a call

#define enablePin 2
#define batteryTestPin 3
#define buttonPin 4 
#define alarm4Pin 4 
#define alarm3Pin 5
#define alarm2Pin 6
#define alarm1Pin 7 // Inboard motion detector 
#define doorPin	8
#define callPin	9
#define sensorVccPin 10
#define sensorGndPin 11
#define gsmPowerPin 12
#define ledPin 13

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

// Sequence:
#define SEQ_DISABLED 0     
#define SEQ_ENABLING 1
#define SEQ_ENABLED 2     
#define SEQ_ALARM 3      
#define SEQ_CALLING 4      
#define SEQ_CALLED 5      
unsigned int sequence = SEQ_DISABLED;

#define NONE 0
#define ALARM1 1
#define ALARM2 2
#define ALARM3 3
#define ALARM4 4
#define DOORALARM 5
#define DAILYCALL 6
unsigned int alarmCause = NONE;

volatile boolean watchdog = 1;
volatile boolean alarm = 0;
double averageLight = 30;

int BH1750address = 0x23;
byte buff[2];
uint16_t light = 0;
unsigned int dayTime = 0;
boolean called = 0;

unsigned long buttonPressStartTime = 0;
unsigned long sequenceStartTime = 0;
unsigned long callingMinTime = 0;
boolean buttonDisabled = false;
boolean forceLed = false;

int watchdogCount = 2;

Blink led;

int BH1750_Read(int address)
{
  int i = 0;
  Wire.beginTransmission(address);
  Wire.requestFrom(address, 2);
  while (Wire.available())
  {
    buff[i] = Wire.read();  // receive one byte
    i++;
  }
  Wire.endTransmission();
  return i;
}

void BH1750_Init(int address)
{
  Wire.beginTransmission(address);
  Wire.write(0x10);//1lx reolution 120ms
  Wire.endTransmission();
}

void BH1750_PowerOn() {
  pinMode(sensorGndPin, OUTPUT);
  digitalWrite(sensorGndPin, LOW);
  pinMode(sensorVccPin, OUTPUT);
  digitalWrite(sensorVccPin, HIGH);
}

void BH1750_PowerOff() {
  pinMode(sensorGndPin, INPUT);
  digitalWrite(sensorGndPin, LOW);
  pinMode(sensorVccPin, INPUT);
  digitalWrite(sensorVccPin, LOW);
}

void ledOn(){
  if (ledEnabled){
    pinMode(ledPin, OUTPUT);
    led.On();
  }
}

void ledOff(){
  if (ledEnabled){
    led.Off();
    pinMode(ledPin, INPUT);
  }
}

void ledBlink(unsigned int onTime, unsigned int totaltime){
  if (ledEnabled){
    pinMode(ledPin, OUTPUT); 
    led.Execute(onTime, totaltime);
  }
} 

unsigned int getNumberOfSms(){
  unsigned int smsnumber = 0; 
  if (alarmCause == DAILYCALL){
    smsnumber++;
    if(!enableBatteryTest || digitalRead(batteryTestPin) == HIGH) {
      return smsnumber;
    }

    smsnumber++;
  }
  else {
    if (hasDoorContact){
      smsnumber++;  
      if (alarmCause == DOORALARM){
        return smsnumber;
      }       
    }
    if (hasAlarm1){
      smsnumber++;  
      if (alarmCause == ALARM1){
        return smsnumber;
      }       
    }
    if (hasAlarm2){
      smsnumber++;  
      if (alarmCause == ALARM2){
        return smsnumber;
      }       
    }
    if (hasAlarm3){
      smsnumber++;  
      if (alarmCause == ALARM3){
        return smsnumber;
      }       
    }
    if (hasAlarm4){
      smsnumber++;  
      if (alarmCause == ALARM4){
        return smsnumber;
      }       
    }
  }

  return smsnumber;
}

void setup() {
  //Initialize the serial port, wire library and RTC module
  Serial.begin(9600);

  //To reduce power, setup all pins as inputs with no pullups
  for (int x = 1 ; x < 18 ; x++) {
    pinMode(x, INPUT);
    digitalWrite(x, LOW);
  }

  digitalWrite(enablePin, HIGH);

  pinMode(callPin, OUTPUT);
  digitalWrite(callPin, LOW);

  if (hasAlarm1){
    digitalWrite(alarm1Pin, HIGH);
  }
  if (hasAlarm2){
    digitalWrite(alarm2Pin, HIGH);
  }
  if (hasAlarm3){
    digitalWrite(alarm3Pin, HIGH);
  }
  if (hasAlarm4){
    digitalWrite(alarm4Pin, HIGH);
  }
  if (hasButton){
    digitalWrite(buttonPin, HIGH);
  }

  // CPU Sleep Modes
  // SM2 SM1 SM0 Sleep Mode
  // 0    0  0 Idle
  // 0    0  1 ADC Noise Reduction
  // 0    1  0 Power-down
  // 0    1  1 Power-save
  // 1    0  0 Reserved
  // 1    0  1 Reserved
  // 1    1  0 Standby(1)
  cbi( SMCR, SE );     // sleep enable, power down mode
  cbi( SMCR, SM0 );    // power down mode
  sbi( SMCR, SM1 );    // power down mode
  cbi( SMCR, SM2 );    // power down mode

  //power_twi_disable();
  //power_spi_disable();
  //power_usart0_disable();
  //power_timer0_disable(); //Needed for delay_ms
  //power_timer1_disable();
  //power_timer2_disable();

  ADCSRA &= ~(1 << ADEN); //Disable ADC
  ACSR = (1 << ACD); //Disable the analog comparator

  //Setup external INT0 interrupt
  EICRA = (1 << ISC01); //Interrupt on falling edge
  EIMSK = (1 << INT0); //Enable INT0 interrupt

  sei(); //Enable global interrupts

  if (hasLightSensor) {
    setup_watchdog(9);
  }

  if (ledEnabled){
    led.Setup(ledPin, false);
  }

  Serial.println();
  Serial.print("Setup");
}

void loop() {
  boolean buttonPressed = false;
  unsigned long currentTime;
  unsigned int numberOfSms;

  if (alarm == 1) {
    alarm = 0;
    delay(400);
    if (hasAlarm1 && digitalRead(alarm1Pin) == LOW) {
      if (debug) {
        Serial.println();
        Serial.print("alarm1");
      }
      alarmCause = ALARM1;
    }
    else if (hasAlarm2 && digitalRead(alarm2Pin) == LOW) {
      if (debug) {
        Serial.println();
        Serial.print("alarm2");
      }
      alarmCause = ALARM2;
    }
    else if (hasAlarm3 && digitalRead(alarm3Pin) == LOW) {
      if (debug) {
        Serial.println();
        Serial.print("alarm3");
      }
      alarmCause = ALARM3;
    }
    else if (hasAlarm4 && digitalRead(alarm4Pin) == LOW) {
      if (debug) {
        Serial.println();
        Serial.print("alarm4");
      }
      alarmCause = ALARM4;
    }
    else if (hasDoorContact && digitalRead(doorPin) == LOW) {
      if (debug) {
        Serial.println();
        Serial.print("Door Alarm");
      }
      alarmCause = DOORALARM;
    }
  }
  else if (watchdog == 1) {
    watchdog = 0;
    if (debug) {
      Serial.print("watchdog...->");
    }
    if (hasLightSensor && sequence == SEQ_ENABLED) {
        watchdogCount--;
        if (watchdogCount <= 0) {
          watchdogCount = 45;

          // Take on sensorOn
          BH1750_PowerOn();
          delay(200);
          Wire.begin();
          BH1750_Init(BH1750address);
          delay(200);

          if (2 == BH1750_Read(BH1750address))
          {
            BH1750_PowerOff();

            light = ((buff[0] << 8) | buff[1]) / 1.2;

            // Average the luminosity in 96h
            averageLight += (light - averageLight) / 400;

            if (light > averageLight) {
              // day
              if (called == 1) {
                // Already called
                dayTime = dailyCallDelay / 2;
              }
              else if (dayTime > dailyCallDelay) {
                // Daily call
                if (debug) {
                  Serial.println();
                  Serial.print("daily call");
                }
                alarmCause = DAILYCALL;
                called = 1;
              }
              else {
                dayTime += 360;
              }
            }
            else {
              // night
              if (dayTime > 360) {
                dayTime -= 360;
              }
              else {
                dayTime = 0;
                called = 0;
              }
            }
          }
          else {
            BH1750_PowerOff();
          }

          if (debug) {
            Serial.println();
            Serial.print("light: ");
            Serial.println(light);
            Serial.print("averageLight: ");
            Serial.println(averageLight);
            Serial.print("dayTime: ");
            Serial.print(dayTime);
            Serial.print("  called: ");
            if (called == 1) {
              Serial.print("yes");
            }
            else {
              Serial.print("no");
            }
            Serial.println();
          }
          delay(50);
        }
      else {
        watchdogCount = 2;
      }
    }
    system_sleep();
    return;
  }

  if (hasButton) {
    if (digitalRead(buttonPin) == LOW){
      if (buttonPressStartTime == 0){
        buttonPressStartTime = millis();
      }
    } 
    else {
      buttonDisabled = false;
      buttonPressStartTime = 0;
    }
    buttonPressed = !buttonDisabled && buttonPressStartTime > 0 && millis() - buttonPressStartTime > buttonPressMinTime;  

    if (buttonPressed) {
      buttonDisabled = true;
      if (debug) {
        Serial.println();
        Serial.print("Button Pressed");
      }
    }
  }

  switch (sequence)
  {  
  case SEQ_DISABLED:	
    if (hasButton){
      if (alarmCause != NONE || buttonPressStartTime > 0 || forceLed) {
        if (alarmCause != NONE || forceLed) {
          ledOn();
        }
        else{
          ledOff();
        }
        if (sequenceStartTime == 0){
          sequenceStartTime = millis();
        }

        // Calc elapsed time in seconds
        currentTime = (millis() - sequenceStartTime) / 1000;      
        if (currentTime > ledOnAlarmMinTime) {
          system_sleep();        
        }
      }
      else {      
        system_sleep();
        break;
      }    

      if (!buttonPressed){
        break;
      }
    }

    // Post-sequence
    buttonPressed = false;
    sequenceStartTime = 0;
    ledOff();
    delay(200);
    sequence++;  

  case SEQ_ENABLING:
    // Pre-sequence
    if (sequenceStartTime == 0){
      if (debug) {
        Serial.println();
        Serial.print("Enabling...");
      }
      sequenceStartTime = millis();
    }

    // Calc elapsed time in seconds
    currentTime = (millis() - sequenceStartTime) / 1000;

    if (buttonPressStartTime == 0) {
      ledBlink(400, 600);
    }

    if (buttonPressed) {
      buttonPressed = false;
      sequence = SEQ_DISABLED;
      break;
    }

    if(currentTime < enablingMinTime){
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    alarmCause = NONE;
    sequence++;
    ledOff();

  case SEQ_ENABLED:
    // Pre-sequence
    if (sequenceStartTime == 0){
      if (debug) {
        Serial.println();
        Serial.print("Enabled...");
      }
      //sequenceStartTime = millis();
    }      

    if (buttonPressed) {
      buttonPressed = false;
      sequence = SEQ_DISABLED;
      break;
    }

    if (alarmCause == NONE) {
      system_sleep();
      break;
    }

    // There is an alarm
    sequenceStartTime = 0;
    sequence++;

  case SEQ_ALARM:
    // Pre-sequence
    if (sequenceStartTime == 0){
      if (debug) {
        Serial.println();
        Serial.print("Alarm...");
      }
      // Relay on and battery sensor on
      digitalWrite(gsmPowerPin, HIGH);
      sequenceStartTime = millis();
    }

    // Calc elapsed time in seconds
    currentTime = (millis() - sequenceStartTime) / 1000;

    if (buttonPressed){
      digitalWrite(gsmPowerPin, LOW);
      buttonPressed = false;
      sequence = SEQ_DISABLED;
      forceLed = true;
      break;
    }

    if(currentTime < detectedMinTime)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sequence++;    

  case SEQ_CALLING:
    if (sequenceStartTime == 0){
      // Calc calling min time from alarm and battery state
      numberOfSms = getNumberOfSms();
      if (debug) {
        Serial.println();
        Serial.print("Calling...");
        Serial.print("Sending " + String(numberOfSms) + "sms");
      }
      callingMinTime = numberOfSms * smsDelayTime;      
      sequenceStartTime = millis();
    }

    // Take on alarm output or GSM calling
    digitalWrite(callPin, HIGH);

    // Calc elapsed time in seconds
    currentTime = (millis() - sequenceStartTime) / 1000;
    if(currentTime < callingMinTime && !buttonPressed)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sequence++;  

  case SEQ_CALLED:
    if (sequenceStartTime == 0){
      if (debug) {
        Serial.println();
        Serial.print("Called...");
      }
      sequenceStartTime = millis();
    }

    if (alarmCause != NONE){
      alarmCause = NONE;
      sequenceStartTime = millis();
    }

    digitalWrite(callPin, LOW);

    if (buttonPressed) {
      digitalWrite(gsmPowerPin, LOW);
      buttonPressed = false;
      sequence = SEQ_DISABLED;
      forceLed = true;
      break;
    }

    // Calc elapsed time in seconds
    currentTime = (millis() - sequenceStartTime) / 1000;
    if(currentTime < calledMinTime || alarmCause == DAILYCALL)
    {
      break;
    }

    digitalWrite(gsmPowerPin, LOW);
    sequence = SEQ_ENABLING;
    sequenceStartTime = 0;
    break;
  }
}

// set system into the sleep state
// system wakes up when wtchdog is timed out
void system_sleep() {
  if (debug) {
    Serial.println();
    Serial.print("System Sleep");
    delay(200);
  }

  forceLed = false;
  alarmCause = NONE;
  sequenceStartTime = 0;
  ledOff();

  cbi(ADCSRA, ADEN);                   // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();

  sleep_mode();                        // System sleeps here

    sleep_disable();                   // System continues execution here when watchdog timed out
  sbi(ADCSRA, ADEN);                   // switch Analog to Digitalconverter ON
}

// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii) {

  byte bb;
  int ww;
  if (ii > 9 ) {
    ii = 9;
  }
  bb = ii & 7;
  if (ii > 7) {
    bb |= (1 << 5);
  }
  bb |= (1 << WDCE);
  ww = bb;

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when  watchdog timed out
ISR(WDT_vect) {
  watchdog = 1; // set global flag
}

SIGNAL(INT0_vect) {
  alarm = 1; // set global flag
}


