#define ledPin 13
#define motionPin 7
#define relayPin 12

void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(motionPin, INPUT);
  pinMode(relayPin, OUTPUT);
  
  digitalWrite(motionPin, HIGH);
  digitalWrite(relayPin, LOW);
}

void loop(){
  unsigned int sensorState = digitalRead(motionPin) == HIGH ? LOW : HIGH;
  digitalWrite(ledPin, sensorState);  
  digitalWrite(relayPin, sensorState);
}
