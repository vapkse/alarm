#define ledPin 13
#define batteryDetectorPin 3

void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(batteryDetectorPin, INPUT);
}

void loop(){
  unsigned int batteryDetectorState = digitalRead(batteryDetectorPin) == HIGH ? LOW : HIGH;
  digitalWrite(ledPin, batteryDetectorState);  
}
